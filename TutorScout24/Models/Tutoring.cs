﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorScout24.Models
{
    public class Tutoring
    {
        public string Type { get; set; }
        public string CreationDate { get; set; }
        public string Creator { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
    }
}
